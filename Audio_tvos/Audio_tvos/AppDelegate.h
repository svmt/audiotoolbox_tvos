//
//  AppDelegate.h
//  Audio_tvos
//
//  Created by Igor Nazarov on 04.08.2020.
//  Copyright © 2020 Sceenic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

