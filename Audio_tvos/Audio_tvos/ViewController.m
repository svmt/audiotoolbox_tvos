//
//  ViewController.m
//  Audio_tvos
//
//  Created by Igor Nazarov on 04.08.2020.
//  Copyright © 2020 Sceenic. All rights reserved.
//

#import "ViewController.h"
#import <AudioToolbox/AudioToolbox.h>

// A VP I/O unit's bus 1 connects to input hardware (microphone).
static const AudioUnitElement kInputBus = 1;
// A VP I/O unit's bus 0 connects to output hardware (speaker).
static const AudioUnitElement kOutputBus = 0;

static const UInt32 kBytesPerSample = 2;

@interface ViewController () {
    AudioUnit vpio_unit_;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createAudioUnit];
//    [self enableAudioInput];
    [self enableAudioOutput];
    [self setupRenderCallback];
    [self disableBufferAllocation];
//    [self setupInputCallback];
    [self setupFormat];
}

- (void)createAudioUnit {
    // Create an audio component description to identify the Voice Processing
    // I/O audio unit.
    AudioComponentDescription vpio_unit_description;
    vpio_unit_description.componentType = kAudioUnitType_Output;
    vpio_unit_description.componentSubType = kAudioUnitSubType_GenericOutput;
    vpio_unit_description.componentManufacturer = kAudioUnitManufacturer_Apple;
    vpio_unit_description.componentFlags = 0;
    vpio_unit_description.componentFlagsMask = 0;
    
    // Obtain an audio unit instance given the description.
    AudioComponent found_vpio_unit_ref =
    AudioComponentFindNext(NULL, &vpio_unit_description);
    
    // Create a Voice Processing IO audio unit.
    OSStatus result = noErr;
    result = AudioComponentInstanceNew(found_vpio_unit_ref, &vpio_unit_);
    if (result != noErr) {
        vpio_unit_ = NULL;
        NSLog(@"AudioComponentInstanceNew failed. Error=%ld.", (long)result);
    }
}

- (void)enableAudioInput {
    // Enable input on the input scope of the input element.
    UInt32 enable_input = 1;
    OSStatus result = noErr;
    result = AudioUnitSetProperty(vpio_unit_, kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Input, kInputBus, &enable_input,
                                  sizeof(enable_input));
    if (result != noErr) {
        NSLog(@"Failed to enable input on input scope of input element. "
              "Error=%ld.",
              (long)result);
    }
}

- (void)enableAudioOutput {
    // Enable output on the output scope of the output element.
    UInt32 enable_output = 1;
    OSStatus result = noErr;
    result = AudioUnitSetProperty(vpio_unit_, kAudioOutputUnitProperty_EnableIO,
                                  kAudioUnitScope_Output, kOutputBus,
                                  &enable_output, sizeof(enable_output));
    if (result != noErr) {
        NSLog(@"Failed to enable output on output scope of output element. "
              "Error=%ld.",
              (long)result);
    }
}

- (void)setupRenderCallback {
    // Specify the callback function that provides audio samples to the audio
    // unit.
    AURenderCallbackStruct render_callback;
    render_callback.inputProc = OnGetPlayoutData;
    render_callback.inputProcRefCon = (__bridge void * _Nullable)(self);
    OSStatus result = noErr;
    result = AudioUnitSetProperty(
                                  vpio_unit_, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Input,
                                  kOutputBus, &render_callback, sizeof(render_callback));
    if (result != noErr) {
        NSLog(@"Failed to specify the render callback on the output bus. "
              "Error=%ld.",
              (long)result);
    }
}

- (void)disableBufferAllocation {
    // Disable AU buffer allocation for the recorder, we allocate our own.
    // TODO(henrika): not sure that it actually saves resource to make this call.
    UInt32 flag = 0;
    OSStatus result = noErr;
    result = AudioUnitSetProperty(
                                  vpio_unit_, kAudioUnitProperty_ShouldAllocateBuffer,
                                  kAudioUnitScope_Output, kInputBus, &flag, sizeof(flag));
    if (result != noErr) {
        NSLog(@"Failed to disable buffer allocation on the input bus. "
              "Error=%ld.",
              (long)result);
    }
}

- (void)setupInputCallback {
    // Specify the callback to be called by the I/O thread to us when input audio
    // is available. The recorded samples can then be obtained by calling the
    // AudioUnitRender() method.
    AURenderCallbackStruct input_callback;
    input_callback.inputProc = OnDeliverRecordedData;
    input_callback.inputProcRefCon = (__bridge void * _Nullable)(self);
    OSStatus result = noErr;
    result = AudioUnitSetProperty(vpio_unit_,
                                  kAudioOutputUnitProperty_SetInputCallback,
                                  kAudioUnitScope_Global, kInputBus,
                                  &input_callback, sizeof(input_callback));
    if (result != noErr) {
        NSLog(@"Failed to specify the input callback on the input bus. "
              "Error=%ld.",
              (long)result);
    }
}

- (void)setupFormat {
    // Set the format on the output scope of the input element/bus.
    Float64 sample_rate = 48000;
    OSStatus result = noErr;
    AudioStreamBasicDescription format = [self getFormatForSampleRate:sample_rate];
    UInt32 size = sizeof(format);
    
    result =
    AudioUnitSetProperty(vpio_unit_, kAudioUnitProperty_StreamFormat,
                         kAudioUnitScope_Output, kInputBus, &format, size);
    if (result != noErr) {
        NSLog(@"Failed to set format on output scope of input bus. "
              "Error=%ld.",
              (long)result);
    }
    
    // Set the format on the input scope of the output element/bus.
    result =
    AudioUnitSetProperty(vpio_unit_, kAudioUnitProperty_StreamFormat,
                         kAudioUnitScope_Input, kOutputBus, &format, size);
    if (result != noErr) {
        NSLog(@"Failed to set format on input scope of output bus. "
              "Error=%ld.",
              (long)result);
    }
}

- (AudioStreamBasicDescription)getFormatForSampleRate:
    (Float64)sample_rate {
  // Set the application formats for input and output:
  // - use same format in both directions
  // - avoid resampling in the I/O unit by using the hardware sample rate
  // - linear PCM => noncompressed audio data format with one frame per packet
  // - no need to specify interleaving since only mono is supported
  AudioStreamBasicDescription format;
  format.mSampleRate = sample_rate;
  format.mFormatID = kAudioFormatLinearPCM;
  format.mFormatFlags =
      kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
  format.mBytesPerPacket = kBytesPerSample;
  format.mFramesPerPacket = 1;  // uncompressed.
  format.mBytesPerFrame = kBytesPerSample;
  format.mChannelsPerFrame = 1;
  format.mBitsPerChannel = 8 * kBytesPerSample;
  return format;
}

OSStatus OnGetPlayoutData(
                          void* in_ref_con,
                          AudioUnitRenderActionFlags* flags,
                          const AudioTimeStamp* time_stamp,
                          UInt32 bus_number,
                          UInt32 num_frames,
                          AudioBufferList* io_data) {
    return 0;
}

OSStatus OnDeliverRecordedData(
                               void* in_ref_con,
                               AudioUnitRenderActionFlags* flags,
                               const AudioTimeStamp* time_stamp,
                               UInt32 bus_number,
                               UInt32 num_frames,
                               AudioBufferList* io_data) {
    return 0;
}

@end
